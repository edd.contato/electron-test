// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
const {ipcRenderer} = require('electron');

window.addEventListener('message', event => {
    // do something with custom event
    const message = event.data;

    if (message.action === 'read-file') {
        ipcRenderer.send('read-file', message.path);
    }

    if (message.action === 'close-app') {
        ipcRenderer.send('close-app');
    }
});

ipcRenderer.on('read-file-response', (event, text) => {
    document.getElementById('text').innerText = text;
});
